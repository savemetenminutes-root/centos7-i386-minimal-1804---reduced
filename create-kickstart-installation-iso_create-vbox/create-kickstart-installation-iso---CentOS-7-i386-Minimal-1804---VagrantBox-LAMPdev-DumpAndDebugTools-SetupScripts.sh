#!/usr/bin/env bash

yum install -y genisoimage;

if [ -f /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804.iso ]; then
    if [ "$(sha256sum /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804.iso | cut -d' ' -f1 | tr -d '\r')" != "$(curl -s http://mirror.centos.org/altarch/7/isos/i386/sha256sum.txt | grep CentOS-7-i386-Minimal-1804.iso | cut -d' ' -f1 | tr -d '\r')" ]; then
        echo 'The checksum of the existing installation ISO file does not match with the one provided online. Replacing the existing file with a newly downloaded one...';
        rm -f /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804.iso;
        wget --directory=/home/VirtualBox/iso http://mirror.centos.org/altarch/7/isos/i386/CentOS-7-i386-Minimal-1804.iso;
    else
        echo 'The installation ISO file already exists and its checksum matches the one provided online. Skipping download.';
    fi;
else
    echo 'Installation ISO file missing... Downloading...';
    wget --directory=/home/VirtualBox/iso http://mirror.centos.org/altarch/7/isos/i386/CentOS-7-i386-Minimal-1804.iso;
fi;
mkdir -p /home/VirtualBox/iso/mountpoint-CentOS-7-i386-Minimal-1804;
echo 'Mounting the original installation ISO file and extracting its contents...';
mount -o loop /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804.iso /home/VirtualBox/iso/mountpoint-CentOS-7-i386-Minimal-1804;
mkdir -p /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804;
rm -rf /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804/*;
shopt -s dotglob;
cp -avRf /home/VirtualBox/iso/mountpoint-CentOS-7-i386-Minimal-1804/* /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804;
echo 'Unmounting ISO file...';
umount /home/VirtualBox/iso/mountpoint-CentOS-7-i386-Minimal-1804;

# https://www.centos.org/docs/5/html/Installation_Guide-en-US/ch-kickstart2.html
# https://www.tldp.org/HOWTO/Partition/requirements.html
# https://pykickstart.readthedocs.io/en/latest/kickstart-docs.html
# https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/installation_guide/sect-automated-installation
# http://sudhaker.com/32/my-kickstart-package-selection-for-centos-7-2
echo 'Creating the Anaconda kickstart config file...';
cat > /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804/ks.cfg << 'EOL'
#version=CentOS7

# Action
install

# Use CDROM installation media
cdrom

# Run the Setup Agent on first boot
firstboot --enable

# Accept Eula
eula --agreed

# Use text mode install
#text
# Use graphical install
#graphical

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'

# System language
lang en_US.UTF-8

# Root password
rootpw --iscrypted $6$BIXWvziM2YRNnZwT$Nhi8c1Y8UoN0vAOqU.psHbCxx8SeclXHQPCVL5PreFS2QaDuFrmnLiQe2Zn3F50fXG0duYByrRu0Us5RmH6vB/
#user --groups=wheel --homedir=/home/vagrant --name=marc --password=$1$XlGhvW09$UbKYTjURWYSAn6nFXmSJw0 --iscrypted --gecos="vagrant"

# System language
lang en_US

# System timezone
timezone Etc/UTC --isUtc --nontp

# Clear the Master Boot Record
zerombr

# System bootloader configuration
bootloader --location=mbr --boot-drive=sda

# Partition clearing information
clearpart --all --initlabel --drives=sda
ignoredisk --only-use=sda

# Disk partitioning information (provide at least 14GB total HDD space)
partition /boot --fstype="ext4" --ondisk=sda --size=512
partition swap --fstype="swap" --ondisk=sda --size=1024
partition pv.01 --fstype="lvmpv" --ondisk=sda --grow
volgroup system --pesize=4096 pv.01
logvol / --vgname=system --name="root" --fstype="ext4" --size=5120 --grow
partition /install --fstype="ext4" --ondisk=sda --size=7168
#partition pv.02 --fstype="lvmpv" --ondisk=sda
#volgroup data --pesize=4096 pv.02
#logvol /install --vgname=data --name="install"  --fstype="ext4" --size=7168

# Network information
network  --bootproto=dhcp --device=enp0s3 --onboot=on --ipv6=auto --activate
network  --bootproto=dhcp --device=enp0s8 --onboot=on --ipv6=auto --activate
network  --hostname=localhost.localdomain

# Firewall configuration
firewall --disabled

# System authorization information
auth  --useshadow  --passalgo=sha512

# System services
services --enabled=NetworkManager,sshd,chronyd

# SELinux configuration
#selinux --enforcing
selinux --permissive

# Do not configure the X Window System
skipx

# Reboot/shutdown after installation (omit to keep the installation running at the completion screen)
#reboot
#halt
#shutdown
poweroff

repo --name="Base" --baseurl="http://mirror.centos.org/altarch/7/os/i386/"
repo --name="Extras" --baseurl="http://mirror.centos.org/altarch/7/extras/i386/"
repo --name="Updates" --baseurl="http://mirror.centos.org/altarch/7/updates/i386/"
#url --url http://mirror.centos.org/altarch/7/os/i386/Packages/
#url --url http://mirror.centos.org/altarch/7/os/i386/Packages/git-1.8.3.1-13.el7.i686.rpm

%packages
@^minimal
@core
kexec-tools

libcurl-devel
git
%end

%addon com_redhat_kdump --enable --reserve-mb='auto'
%end

%post --log=/root/ks-post.log
mkdir -p /install/scripts;
git clone https://github.com/savemetenminutes/setup-centos7-i386-lamp.git /install/scripts;
chmod +x /install/scripts/*.sh;
%end

%anaconda
pwpolicy root --minlen=6 --minquality=1 --notstrict --nochanges --notempty
pwpolicy user --minlen=6 --minquality=1 --notstrict --nochanges --emptyok
pwpolicy luks --minlen=6 --minquality=1 --notstrict --nochanges --notempty
%end
EOL

#label linux
#  menu label ^Install CentOS 7
#  kernel vmlinuz
#  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS\x207\x20i686 quiet

#label kickstart
#  menu label ^Kickstart Installation of RHEL7.3
#  kernel vmlinuz
#  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS\x207\x20i686 inst.ks=cdrom:/ks.cfg

isoLabel=$(isoinfo -d -i /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804.iso | grep "Volume id" | sed -e 's/Volume id: //');
isoLabelEncoded=$(echo ${isoLabel} | sed -e 's/ /\\x20/g');
echo 'Backing up the original ISOLINUX boot loader config file...';
mv /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804/isolinux/isolinux.cfg /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804/isolinux/isolinux.cfg.dist;
echo 'Creating an ISOLINUX boot loader config file which automatically starts the installation...';
cat > /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804/isolinux/isolinux.cfg << EOL
default kickstart

label kickstart
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=hd:LABEL=${isoLabelEncoded} inst.ks=cdrom:/ks.cfg quiet

menu end
EOL

cat > /dev/null << 'EOL'
default vesamenu.c32
timeout 600

display boot.msg

# Clear the screen when exiting the menu, instead of leaving the menu displayed.
# For vesamenu, this means the graphical background is still displayed without
# the menu itself for as long as the screen remains in graphics mode.
menu clear
menu background splash.png
menu title CentOS 7
menu vshift 8
menu rows 18
menu margin 8
#menu hidden
menu helpmsgrow 15
menu tabmsgrow 13

# Border Area
menu color border * #00000000 #00000000 none

# Selected item
menu color sel 0 #ffffffff #00000000 none

# Title bar
menu color title 0 #ff7ba3d0 #00000000 none

# Press [Tab] message
menu color tabmsg 0 #ff3a6496 #00000000 none

# Unselected menu item
menu color unsel 0 #84b8ffff #00000000 none

# Selected hotkey
menu color hotsel 0 #84b8ffff #00000000 none

# Unselected hotkey
menu color hotkey 0 #ffffffff #00000000 none

# Help text
menu color help 0 #ffffffff #00000000 none

# A scrollbar of some type? Not sure.
menu color scrollbar 0 #ffffffff #ff355594 none

# Timeout msg
menu color timeout 0 #ffffffff #00000000 none
menu color timeout_msg 0 #ffffffff #00000000 none

# Command prompt text
menu color cmdmark 0 #84b8ffff #00000000 none
menu color cmdline 0 #ffffffff #00000000 none

# Do not display the actual menu unless the user presses a key. All that is displayed is a timeout message.

menu tabmsg Press Tab for full configuration options on menu items.

menu separator # insert an empty line
menu separator # insert an empty line

label linux
  menu label ^Install CentOS 7
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS\x207\x20i686 quiet

label check
  menu label Test this ^media & install CentOS 7
  menu default
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS\x207\x20i686 rd.live.check quiet

menu separator # insert an empty line

# utilities submenu
menu begin ^Troubleshooting
  menu title Troubleshooting

label vesa
  menu indent count 5
  menu label Install CentOS 7 in ^basic graphics mode
  text help
	Try this option out if you're having trouble installing
	CentOS 7.
  endtext
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS\x207\x20i686 xdriver=vesa nomodeset quiet

label rescue
  menu indent count 5
  menu label ^Rescue a CentOS system
  text help
	If the system will not boot, this lets you access files
	and edit config files to try to get it booting again.
  endtext
  kernel vmlinuz
  append initrd=initrd.img inst.stage2=hd:LABEL=CentOS\x207\x20i686 rescue quiet

label memtest
  menu label Run a ^memory test
  text help
	If your system is having issues, a problem with your
	system's memory may be the cause. Use this utility to
	see if the memory is working correctly.
  endtext
  kernel memtest

menu separator # insert an empty line

label local
  menu label Boot from ^local drive
  localboot 0xffff

menu separator # insert an empty line
menu separator # insert an empty line

label returntomain
  menu label Return to ^main menu
  menu exit

menu end
EOL

if [ -f /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804---VagrantBox-LAMPdev-DumpAndDebugTools-SetupScripts.iso ]; then
    echo 'Removing the existing repackaged installation ISO file...';
    rm -f /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804---VagrantBox-LAMPdev-DumpAndDebugTools-SetupScripts.iso;
fi;
echo 'Repackaging the new installation ISO file...';
#mkisofs|genisoimage
genisoimage \
    -r \
    -R \
    -J \
    -T \
    -v \
    -no-emul-boot \
    -boot-load-size 4 \
    -boot-info-table \
    -V "${isoLabel}" \
    -p "Milen Kirilov (SaveMeTenMinutes)" \
    -A "${isoLabel} VagrantBox, LAMP development, Dump&Debug tools - setup scripts - $(date --utc +%Y%m%d_%H%M%SZ)" \
    -b isolinux/isolinux.bin \
    -c isolinux/boot.cat \
    -x "lost+found" \
    --joliet-long \
    -o /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804---VagrantBox-LAMPdev-DumpAndDebugTools-SetupScripts.iso \
/home/VirtualBox/iso/CentOS-7-i386-Minimal-1804;

exit 0;