#!/usr/bin/env bash

echo "##########################################################";
echo "# CURRENTLY THE SCRIPT CAN ONLY RUN EXECUTABLES IN /bin!!!";
echo "##########################################################";
echo "Script control commands:";
echo "reset         Clears the current target VirtualBox and lets you choose a new one.";
echo "exit          Terminates the current session and exits the script.";
echo '';
echo '';
echo '';

command='';
while [ "${command}" != "exit" ]; do
    declare -A runningVboxes;
    i=0;
    while IFS=" " read -r name uuid; do
        (( i++ ));
        runningVboxes[${i}":name"]=${name};
        runningVboxes[${i}":uuid"]=${uuid};
    done < <(vboxmanage list vms)

    j=0;
    for ((j=1;j<=i;j++)); do
        echo '['${j}'] '${runningVboxes["${j}:name"]}' '${runningVboxes["${j}:uuid"]};
    done;

    echo '';
    read -p 'Enter a VirtualBox number: ' targetVirtualbox;

    read -p 'Enter username: ' targetVirtualboxUsername;
    while true; do
        read -p 'Specify the user'"'"'s password: ' -s targetVirtualboxPassword;
        echo;
        read -p 'Specify the user'"'"'s password (again): ' -s targetVirtualboxPassword2;
        echo;
        [ "$targetVirtualboxPassword" = "$targetVirtualboxPassword2" ] && break;
        echo "The passwords do not match. Please try again!";
    done;

    command='';
    while [ "${command}" != "exit" ] && [ "${command}" != "reset" ]; do
        targetVirtualboxCurrentDirectory=$(vboxmanage --nologo guestcontrol ${runningVboxes["${targetVirtualbox}:name"]:1:-1} run --username ${targetVirtualboxUsername} --password ${targetVirtualboxPassword} --exe /bin/pwd);
        prompt=${targetVirtualboxUsername}'@'${runningVboxes["${j}:name"]}':'${targetVirtualboxCurrentDirectory};
        read -p ${prompt}$'\n# ' command;

        if [ "${command}" != "exit" ] && [ "${command}" != "reset" ]; then
            command=${command}' ';
            commandUtility=$(echo ${command}' ' | cut -d' ' -f1);
            commandArguments=$(echo ${command}' ' | cut -d' ' -f2-);
            if [[ ${commandArguments} = *[!\ ]* ]]; then
                commandArguments=' -- '${commandUtility}'/arg0 '${commandArguments};
            else
                commandArguments='';
            fi;

            #echo 'vboxmanage --nologo guestcontrol '${runningVboxes["${targetVirtualbox}:name"]:1:-1}' run --username '${targetVirtualboxUsername}' --password '${targetVirtualboxPassword}' --exe /bin/'${commandUtility}${commandArguments};
            vboxmanage --nologo guestcontrol ${runningVboxes["${targetVirtualbox}:name"]:1:-1} run --username ${targetVirtualboxUsername} --password ${targetVirtualboxPassword} --exe /bin/${commandUtility}${commandArguments};
        fi;
    done;
done;

exit 0;