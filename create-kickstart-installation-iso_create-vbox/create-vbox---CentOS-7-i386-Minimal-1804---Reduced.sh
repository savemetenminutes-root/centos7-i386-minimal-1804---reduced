#!/usr/bin/env bash

VM='CentOS-7-i386-Minimal-1804---Reduced';

mkdir -p /home/VirtualBox/boxes/${VM};

# https://www.oracle.com/technetwork/articles/servers-storage-admin/manage-vbox-cli-2264359.html
# https://www.virtualbox.org/manual/ch08.html
# https://www.perkin.org.uk/posts/create-virtualbox-vm-from-the-command-line.html

VBoxManage createhd --variant Standard --filename /home/VirtualBox/boxes/${VM}/disks/${VM}.vdi --size 14336;

#VBoxManage list ostypes;

VBoxManage createvm --name ${VM} --ostype "RedHat" --register --basefolder /home/VirtualBox/boxes/${VM};
# https://www.howtogeek.com/312883/how-to-shrink-a-virtualbox-virtual-machine-and-free-up-disk-space/
# https://www.virtualbox.org/manual/ch08.html#vboxmanage-modifyvdi
# https://www.howtogeek.com/howto/40702/how-to-manage-and-use-lvm-logical-volume-management-in-ubuntu/
# https://www.howtogeek.com/wp-content/uploads/2011/01/lvm-cheatsheet.png
VBoxManage storagectl ${VM} --name "SATA Controller" --add sata --controller IntelAHCI;
VBoxManage storageattach ${VM} --storagectl "SATA Controller" --port 0 --device 0 --type hdd --medium /home/VirtualBox/boxes/${VM}/disks/${VM}.vdi;

VBoxManage storagectl ${VM} --name "IDE Controller" --add ide;
VBoxManage storageattach ${VM} --storagectl "IDE Controller" --port 0 --device 0 --type dvddrive --medium /home/VirtualBox/iso/CentOS-7-i386-Minimal-1804---Reduced.iso;

# VirtualBox 4.* provides the following virtual network cards:[1]
# AMD PCNet PCI II (Am79C970A)
# AMD PCNet FAST III (Am79C973, the default)
# Intel PRO/1000 MT Desktop (82540EM, DEFAULT ON VirtualBox 5.2.18!!!)
# Intel PRO/1000 T Server (82543GC)
# Intel PRO/1000 MT Server (82545EM)
# Paravirtualized network adapter (virtio-net)

# https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/
# https://github.com/systemd/systemd/blob/master/src/udev/udev-builtin-net_id.c#L20
# https://docs.oracle.com/cd/E52668_01/E53499/html/section_xzj_zzp_n4.html
# https://www.virtualbox.org/pipermail/vbox-dev/2013-July/011641.html
# https://www.ispcolohost.com/2015/07/11/kickstarting-with-redhats-inconsistent-network-device-naming/
# https://www.centos.org/forums/viewtopic.php?t=47720
# https://www.golinuxhub.com/2017/07/sample-kickstart-configuration-file-for.html
# https://www.thomas-krenn.com/en/wiki/Network_Configuration_in_VirtualBox
# https://forums.virtualbox.org/viewtopic.php?f=7&t=82163
# The predictable network interface name for this one should be enp0s3
if [[ ! $(vboxmanage list hostonlyifs | grep vboxnet0) ]]; then
    #while read discard name; do
    #    VBoxManage hostonlyif remove ${name};
    #done < <(vboxmanage list hostonlyifs | grep "^Name:");
    # https://superuser.com/questions/429432/how-can-i-configure-a-dhcp-server-assigned-to-a-host-only-net-in-virtualbox
    VBoxManage hostonlyif create vboxnet0;
fi;
# VBoxManage list hostonlyifs;
# VBoxManage list dhcpservers;
VBoxManage modifyvm ${VM} --nic1 nat --nictype1 82540EM --nicproperty1 name="VirtualBox NAT Ethernet Adapter";
# The predictable network interface name for this one should be enp0s8
VBoxManage modifyvm ${VM} --nic2 hostonly --nictype2 82540EM --nicproperty2 name="VirtualBox Host-Only Ethernet Adapter";
# https://forums.virtualbox.org/viewtopic.php?f=7&t=89622&p=429901#p429901
# Problems assigning a dynamic IP address by the DHCP server
# https://www.virtualbox.org/ticket/7905
# https://forums.virtualbox.org/viewtopic.php?f=6&t=37402
# https://forums.virtualbox.org/viewtopic.php?f=3&t=84811
VBoxManage modifyvm ${VM} --hostonlyadapter2 vboxnet0;
# Note that selecting a different network adapter or using a different chipset for the VirtualBox may change the predictable
# network interface names rendering the kickstart installation unable to configure the network adapters properly!!!
#
# In such cases it might be possible to reconfigure/initialize the network interfaces using the
# /install/script/connect-ethernet-devices_autoconnect-ethernet-connections.sh script by gaining control of the box
# using either VDRP (see below on how to enable it) or using:
# ```
# vboxmanage --nologo guestcontrol ${VM} run --username root --password YOUR_ROOT_USER_PASSWORD --exe /install/scripts/connect-ethernet-devices_autoconnect-ethernet-connections.sh;
# ```

VBoxManage modifyvm ${VM} --audio none;
VBoxManage modifyvm ${VM} --usb off;
# https://www.virtualbox.org/manual/ch03.html#settings-motherboard
#VBoxManage modifyvm ${VM} --ioapic on;
VBoxManage modifyvm ${VM} --boot1 dvd --boot2 disk --boot3 none --boot4 none;
VBoxManage modifyvm ${VM} --memory 1024 --vram 16;
VBoxManage modifyvm ${VM} --clipboard bidirectional;
# Add remote control functionality (use super+r "mstsc" in windows to launch the remote desktop connection utility)
#VBoxManage modifyvm ${VM} --vrde on --vrdeport 50000;
# Install the extension pack on the host machine to be able top use the VirtualBox Remote Desktop Protocol
#wget --no-clobber --directory=/install/VirtualBox https://download.virtualbox.org/virtualbox/5.2.18/Oracle_VM_VirtualBox_Extension_Pack-5.2.18.vbox-extpack;
#yes | VBoxManage extpack install --replace /install/VirtualBox/Oracle_VM_VirtualBox_Extension_Pack-5.2.18.vbox-extpack;

VBoxHeadless -s ${VM};

#VBoxManage storageattach ${VM} --storagectl "IDE Controller" --port 0 --device 0 --medium emptydrive;
VBoxManage modifyvm ${VM} --boot1 none --boot2 disk --boot3 none --boot4 none;

# Clone the VirtualBox (the source disk and VirtualBox must not be running)
# https://www.virtualbox.org/manual/ch08.html#vboxmanage-clonevdi
# https://www.virtualbox.org/manual/ch08.html#vboxmanage-clonevm
#mkdir -p /home/VirtualBox/boxes/${VM}-clone/disks;
#VBoxManage clonemedium disk /home/VirtualBox/boxes/${VM}/disks/${VM}.vdi /home/VirtualBox/boxes/${VM}-clone/disks/${VM}.vdi --format VDI --variant Standard
#VBoxManage clonevm ${VM} --name "${VM}-clone" --basefolder /home/VirtualBox/boxes/${VM}-clone --register;

# Export the VirtualBox
# https://www.virtualbox.org/manual/ch08.html#vboxmanage-export
#VBoxManage export ${VM} -o /home/VirtualBox/boxes/${VM}/${VM}.ovf --ovf20

exit 0;